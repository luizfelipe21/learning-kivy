from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label


class Drawning(App):
    def build(self):
        box = BoxLayout(orientation='vertical')
        bt1 = Button(text='SALVE CARAIO', font_size=30,
                     on_release=self.incrementar)
        self.lbl1 = Label(text='1', font_size=30)
        box.add_widget(self.lbl1)
        box.add_widget(bt1)
        return box

    def incrementar(self, button):
        self.lbl1.text = str(int(self.lbl1.text) + 1)


Drawning().run()
